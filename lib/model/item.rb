class Item
  attr_reader :iva, :quantity, :base_price, :description

  def initialize(base_price, type, description, quantity = 1)
    @base_price = base_price
    @iva = iva_for type
    @description = description
    @quantity = quantity
  end

  def total_with_iva
    price_with_iva * @quantity
  end

  def total_without_iva
    price_without_iva * @quantity
  end

  def total_iva
    (total_with_iva - total_without_iva).round 2
  end

  def price_with_iva
    @base_price
  end

  def price_without_iva
    @base_price - (@base_price * @iva)
  end

  private

  def iva_for(type)
    case type.to_sym
    when :sandwich
      0.1
    when :drink
      0.15
    else
      raise ArgumentError.new "Unrecognized item type '#{type}'"
    end
  end
end