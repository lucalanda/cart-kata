class CartView
  def initialize(id, cart)
    @id = id
    @cart = cart
  end

  def to_s
    <<EOS
————————————————————————————————————————————————————
Cestino #{@id}
————————————————————————————————————————————————————
#progressivo_nel_cestino | #descrizione_articolo | #quantita |  #prezzo_unitario | #prezzo_totale_iva_esclusa | #iva | #prezzo_iva_inclusa
#{items_summary @cart}

————————————————————————————————————————————————————
Totale (senza iva): #{format_price @cart.total_without_iva}
Iva applicata: #{format_price @cart.applied_iva}
Totale (iva inclusa): #{format_price @cart.total}
————————————————————————————————————————————————————
EOS
  end

  private

  def items_summary(cart)
    rows = []
    cart.items.each_with_index do |item, index|
      data = [
          index + 1,
          item.description,
          item.quantity,
          format_price(item.price_with_iva),
          format_price(item.total_without_iva),
          format_price(item.total_iva),
          format_price(item.total_with_iva)
      ]

      rows << data.join(' | ')
    end

    rows.join "\n"
  end

  def format_price(price)
    '%.2f' % price + '€'
  end
end
