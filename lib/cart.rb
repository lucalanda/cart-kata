require_relative './items_store'


class Cart
  attr_reader :items

  def initialize(raw_items)
    items_store = ItemsStore.new
    grouped_items = group_raw_items raw_items
    @items = grouped_items.map { |item, count| items_store.get_item(item, count) }
  end

  def total
    total_without_iva + applied_iva
  end

  def total_with_iva
    @items.map(&:total_with_iva).sum.round 2
  end

  def total_without_iva
    @items.map(&:total_without_iva).sum.round 2
  end

  def total_iva
    @items.map(&:total_iva).sum.round 2
  end

  def applied_iva
    total_without_iva > 30 ? 0 : total_iva
  end

  private

  def group_raw_items(raw_items)
    result = Hash.new 0

    raw_items.each { |item| result[item] += 1 }

    result
  end
end