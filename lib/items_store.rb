require 'json'
require_relative './model/item'

ITEMS_FILEPATH = File.join(File.dirname(__FILE__), '../data/items.json')

class ItemsStore
  def initialize
    items_json = File.read(ITEMS_FILEPATH)
    @items = JSON.parse items_json, symbolize_names: true
  end

  def get_item(raw_item, quantity)
    data = @items[raw_item.to_sym]
    Item.new data[:base_price], data[:type], data[:description], quantity
  end
end
