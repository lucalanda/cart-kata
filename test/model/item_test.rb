require 'minitest/autorun'
require_relative '../../lib/model/item.rb'


class ItemTest < MiniTest::Test
  def setup
    @item1 = Item.new 20, "sandwich", "item 1", 1
    @item2 = Item.new 20, "drink", "item 2", 2
  end

  def test_sandwich_type_item_has_correct_iva
    assert_equal 0.10, @item1.iva
    assert_equal 0.15, @item2.iva
  end

  def test_sandwich_throws_error_with_unsupported_type
    assert_raises ArgumentError do
      Item.new 20, :unexisting_type, "item", 1
    end
  end

  def test_item_returns_correct_total_with_iva
    assert_equal 20, @item1.total_with_iva
    assert_equal 40, @item2.total_with_iva
  end

  def test_item_returns_correct_total_without_iva
    assert_equal 18, @item1.total_without_iva
    assert_equal 34, @item2.total_without_iva
  end

  def test_item_returns_correct_total_iva
    assert_equal 2, @item1.total_iva
    assert_equal 6, @item2.total_iva
  end
end
