require 'minitest/autorun'
require_relative '../lib/cart'
require_relative '../lib/cart_view'

ATS_FILEPATH = File.join(File.dirname(__FILE__), './at')

class CartViewTest < MiniTest::Test
  def test_cart1_view
    raw_items = [:hamburger] + [:cocacola] * 4 + [:tramezzini] * 3

    assert_cart_view_is_correct raw_items, 'cart_1.txt', 1
  end

  def test_cart2_view
    raw_items = [:hamburger] * 12

    assert_cart_view_is_correct raw_items, 'cart_2.txt', 2
  end

  def test_cart3_view
    raw_items = [:cocacola, :cocacola, :hamburger]

    assert_cart_view_is_correct raw_items, 'cart_3.txt', 3
  end

  def test_cart4_view
    raw_items = [:cocacola, :hamburger]

    assert_cart_view_is_correct raw_items, 'cart_4.txt', 4
  end

  private

  def assert_cart_view_is_correct(raw_items, expected_output_file, cart_id)
    cart = Cart.new(raw_items)

    expected_output = File.read(File.join(ATS_FILEPATH, expected_output_file))
    cart_view_output = CartView.new(cart_id, cart).to_s

    assert_equal expected_output, cart_view_output
  end
end