require 'minitest/autorun'
require_relative '../lib/cart.rb'

class CartTest < MiniTest::Test

  def after_setup
    @cart1 = Cart.new([:hamburger, :cocacola])
    @cart2 = Cart.new([:hamburger, :hamburger, :hamburger])
    @cart3 = Cart.new([:hamburger, :hamburger, :hamburger, :cocacola])
    @cart4 = Cart.new([:hamburger, :hamburger, :hamburger, :hamburger])
  end

  def test_simple_cart_total
    assert_equal 13, @cart1.total
    assert_equal 30, @cart2.total
    assert_equal 33, @cart3.total
  end

  def test_cart_total_greater_than_30
    assert_equal 36, @cart4.total
  end

  def test_cart_total_without_iva
    assert_equal 11.55, @cart1.total_without_iva
    assert_equal 27, @cart2.total_without_iva
    assert_equal 29.55, @cart3.total_without_iva
    assert_equal 36, @cart4.total_without_iva
  end

  def test_cart_applied_iva_simple_case
    assert_equal 1.45, @cart1.applied_iva
    assert_equal 3, @cart2.applied_iva
    assert_equal 3.45, @cart3.applied_iva
  end

  def test_cart_applied_iva_with_total_greater_than_30
    assert_equal 0, @cart4.applied_iva
  end

end
