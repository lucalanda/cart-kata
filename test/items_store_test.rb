require 'minitest/autorun'
require_relative '../lib/items_store'

class ItemsStoreTest  < MiniTest::Test
  def test_items_store_returns_correct_items
    items_store = ItemsStore.new

    hamburger_item = items_store.get_item :hamburger, 2
    assert_equal 10, hamburger_item.price_with_iva
    assert_equal 0.1, hamburger_item.iva
    assert_equal 2, hamburger_item.quantity
  end
end