# Progetto
* kata sul calcolo del totale di un carrello con eventuale sconto di iva (specifiche in tema.txt)
* sviluppato con ruby 2.6.3 con minitest

# Assunzioni
* ho assunto che il "prezzo_unitario" degli item, riportato nella stampa del carrello, includa l'iva

# Esecuzione
* per eseguire la richiesta del tema, comando "rake" o "rake acceptance_test" nella root del progetto
  * per aggiungere nuovi item supportati modificare data/items.json
* per eseguire i test del codice, comando "rake test" nella root del progetto
